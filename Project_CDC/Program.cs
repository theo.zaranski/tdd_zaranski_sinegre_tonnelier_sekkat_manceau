﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_CDC
{
    public class Program
    {

        public static List<Personne> EquipePoidsTresLegers = new List<Personne>();
        public static List<Personne> EquipePoidsLeger = new List<Personne>();
        public static List<Personne> EquipePoidsLourds = new List<Personne>();
        public static List<Personne> EquipePoidsTresLourds = new List<Personne>();
        public static List<Personne> match1 = new List<Personne>();
        public static List<Personne> match2 = new List<Personne>();

        static void Main(string[] args)
        {
            Creation_Des_Personnes_Par_Defaut();

            Menu_Puis_Afficher_La_Liste_Des_Personnages();
        }

        public static void Ajouter_Personne_A_La_Liste_De_Personnes()
        {
            Console.WriteLine("Entrer le nom :");
            string nom = Console.ReadLine();
            Console.WriteLine("Entrer le prenom :");
            string prenom = Console.ReadLine();
            Console.WriteLine("Entrer l'age :");
            int age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrer le poids :");
            int poids = Convert.ToInt32(Console.ReadLine());
            DateTime anneeAdhesion = DateTime.Now;

            Console.WriteLine("Choisissez votre armure parmis la selection: (Gambison, Mailles, Plaques)");
            string armure = Console.ReadLine();

            Console.WriteLine("Choisissez votre arme : ");
            string arme = Console.ReadLine();

            if (poids < 60) { EquipePoidsTresLegers.Add(new Personne(nom, prenom, age, poids, anneeAdhesion, armure, arme)); }
            else if (poids >= 60 && poids < 80) { EquipePoidsLeger.Add(new Personne(nom, prenom, age, poids, anneeAdhesion, armure, arme)); }
            else if (poids >= 80 && poids < 100) { EquipePoidsLourds.Add(new Personne(nom, prenom, age, poids, anneeAdhesion, armure, arme)); }
            else if (poids >= 100 && poids < 120) { EquipePoidsTresLourds.Add(new Personne(nom, prenom, age, poids, anneeAdhesion, armure, arme)); }

            Console.Clear();
            Menu_Puis_Afficher_La_Liste_Des_Personnages();
        }

        public static void Menu_Puis_Afficher_La_Liste_Des_Personnages()
        {
            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("EquipePoidsTresLegers (<60): ");
            foreach (var personne in EquipePoidsTresLegers)
            { Console.WriteLine(personne.Nom + " " + personne.Prenom + " / " + personne.Age + "ans / " + personne.Poids + "KG / "+"Armure:" + personne.Armure + " / Arme:" + personne.Arme+" / Année d'adhésion:" + personne.AnneeAdhesion); }
            Console.WriteLine("----------------------------------------------------------");

            Console.WriteLine("EquipePoidsLeger (>60 & <80): ");
            foreach (var personne in EquipePoidsLeger)
            { Console.WriteLine(personne.Nom + " " + personne.Prenom + " / " + personne.Age + "ans / " + personne.Poids + "KG / " + "Armure:" + personne.Armure + " / Arme:" + personne.Arme + " / Année d'adhésion:" + personne.AnneeAdhesion); }
            Console.WriteLine("----------------------------------------------------------");

            Console.WriteLine("EquipePoidsLourds (>80 & <100): ");
            foreach (var personne in EquipePoidsLourds)
            { Console.WriteLine(personne.Nom + " " + personne.Prenom + " / " + personne.Age + "ans / " + personne.Poids + "KG / " + "Armure:" + personne.Armure + " / Arme:" + personne.Arme + " / Année d'adhésion:" + personne.AnneeAdhesion); }
            Console.WriteLine("----------------------------------------------------------");

            Console.WriteLine("EquipePoidsTresLourds (>100 & <120): ");
            foreach (var personne in EquipePoidsTresLourds)
            { Console.WriteLine(personne.Nom + " " + personne.Prenom + " / " + personne.Age + "ans / " + personne.Poids + "KG / " + "Armure:" + personne.Armure + " / Arme:" + personne.Arme + " / Année d'adhésion:" + personne.AnneeAdhesion); }
            Console.WriteLine("----------------------------------------------------------");


            /* MENU CHOIX A FAIRE */
            Console.WriteLine("AJOUTER UNE PERSONNE [1], Match[2]");
            var readLine = Console.ReadLine();
            switch (readLine)
            {
                case "1":
                    Ajouter_Personne_A_La_Liste_De_Personnes();
                    break;
                case "2":
                    Partie();
                    break;

                default:
                    break;
            }
        }

        public static void Partie()
        {
            EquipePoidsTresLegers.OrderBy(personne => personne.Poids).ToList();
            Console.WriteLine("Choisissez une equipe entre PoidsTresLegers [1], PoidsLeger [2], PoidsLourds [3], PoidsTresLourds [4]");
            int equipe = Convert.ToInt32(Console.ReadLine());
            if (equipe == 1)
            {
                foreach (var personne in EquipePoidsTresLegers)
                {
                    for (int i = 0; i < EquipePoidsTresLegers.Count; i++)
                    {
                        match1.Add(EquipePoidsTresLegers[i]);
                    }
                }
                Console.WriteLine("Match entre " + EquipePoidsTresLegers[0].Prenom + " et " + EquipePoidsTresLegers[1].Prenom);
                Console.WriteLine("Qui est le gagnant? Joueur [1] ou Joueur [2]");
                int joueur = Convert.ToInt32(Console.ReadLine());
                if (joueur == 1)
                {
                    Console.WriteLine("----------------------------------------------------------");
                    Console.WriteLine("Le gagnant est " + EquipePoidsTresLegers[0].Prenom);
                    Console.WriteLine("----------------------------------------------------------");
                }
                if (joueur == 2)
                {
                    Console.WriteLine("----------------------------------------------------------");
                    Console.WriteLine("Le gagnant est " + EquipePoidsTresLegers[1].Prenom);
                    Console.WriteLine("----------------------------------------------------------");
                }
                Console.WriteLine("Voulez vous revenir au Menu principal [1] ou faire une nouvelle partie [2]?");
                int menu = Convert.ToInt32(Console.ReadLine());
                if (menu == 1)
                {
                    Console.Clear();
                    Menu_Puis_Afficher_La_Liste_Des_Personnages();
                }
                if (menu == 2)
                {
                    Console.Clear();
                    Partie();
                }
            }
            if (equipe == 2)
            {
                foreach (var personne in EquipePoidsLeger)
                {
                    for (int i = 0; i < EquipePoidsLeger.Count; i++)
                    {
                        match1.Add(EquipePoidsLeger[i]);
                    }
                }
                Console.WriteLine("Match entre " + EquipePoidsLeger[0].Prenom + " et " + EquipePoidsLeger[1].Prenom);
                Console.WriteLine("Qui est le gagnant? Joueur [1] ou Joueur [2]");
                int joueur = Convert.ToInt32(Console.ReadLine());
                if (joueur == 1)
                {
                    Console.WriteLine("----------------------------------------------------------");
                    Console.WriteLine("Le gagnant est " + EquipePoidsLeger[0].Prenom);
                    Console.WriteLine("----------------------------------------------------------");
                }
                if (joueur == 2)
                {
                    Console.WriteLine("----------------------------------------------------------");
                    Console.WriteLine("Le gagnant est " + EquipePoidsLeger[1].Prenom);
                    Console.WriteLine("----------------------------------------------------------");
                }
                Console.WriteLine("Voulez vous revenir au Menu principal [1] ou faire une nouvelle partie [2]?");
                int menu = Convert.ToInt32(Console.ReadLine());
                if (menu == 1)
                {
                    Console.Clear();
                    Menu_Puis_Afficher_La_Liste_Des_Personnages();
                }
                if (menu == 2)
                {
                    Console.Clear();
                    Partie();
                }
            }
            if (equipe == 3)
            {
                foreach (var personne in EquipePoidsLourds)
                {
                    for (int i = 0; i < EquipePoidsLourds.Count; i++)
                    {
                        match1.Add(EquipePoidsLourds[i]);
                    }
                }
                Console.WriteLine("Match entre " + EquipePoidsLourds[0].Prenom + " et " + EquipePoidsLourds[1].Prenom);
                Console.WriteLine("Qui est le gagnant? Joueur [1] ou Joueur [2]");
                int joueur = Convert.ToInt32(Console.ReadLine());
                if (joueur == 1)
                {
                    Console.WriteLine("----------------------------------------------------------");
                    Console.WriteLine("Le gagnant est " + EquipePoidsLourds[0].Prenom);
                    Console.WriteLine("----------------------------------------------------------");
                }
                if (joueur == 2)
                {
                    Console.WriteLine("----------------------------------------------------------");
                    Console.WriteLine("Le gagnant est " + EquipePoidsLourds[1].Prenom);
                    Console.WriteLine("----------------------------------------------------------");
                }
                Console.WriteLine("Voulez vous revenir au Menu principal [1] ou faire une nouvelle partie [2]?");
                int menu = Convert.ToInt32(Console.ReadLine());
                if (menu == 1)
                {
                    Console.Clear();
                    Menu_Puis_Afficher_La_Liste_Des_Personnages();
                }
                if (menu == 2)
                {
                    Console.Clear();
                    Partie();
                }
            }
            if (equipe == 4)
            {
                foreach (var personne in EquipePoidsTresLourds)
                {
                    for (int i = 0; i < EquipePoidsTresLourds.Count; i++)
                    {
                        match1.Add(EquipePoidsTresLourds[i]);
                    }
                }
                Console.WriteLine("Match entre " + EquipePoidsTresLourds[0].Prenom + " et " + EquipePoidsTresLourds[1].Prenom);
                Console.WriteLine("Qui est le gagnant? Joueur [1] ou Joueur [2]");
                int joueur = Convert.ToInt32(Console.ReadLine());
                if (joueur == 1)
                {
                    Console.WriteLine("----------------------------------------------------------");
                    Console.WriteLine("Le gagnant est " + EquipePoidsTresLourds[0].Prenom);
                    Console.WriteLine("----------------------------------------------------------");
                }
                if (joueur == 2)
                {
                    Console.WriteLine("----------------------------------------------------------");
                    Console.WriteLine("Le gagnant est " + EquipePoidsTresLourds[1].Prenom);
                    Console.WriteLine("----------------------------------------------------------");
                }
                Console.WriteLine("Voulez vous revenir au Menu principal [1] ou faire une nouvelle partie [2]?");
                int menu = Convert.ToInt32(Console.ReadLine());
                if (menu == 1)
                {
                    Console.Clear();
                    Menu_Puis_Afficher_La_Liste_Des_Personnages();
                }
                if (menu == 2)
                {
                    Console.Clear();
                    Partie();
                }
            }
        }

        public static void Creation_Des_Personnes_Par_Defaut()
        {
            EquipePoidsTresLegers.Add(new Personne("Thierry", "Beccaro", 62, 59, DateTime.Now, "Gambison", "Epée"));
            EquipePoidsTresLegers.Add(new Personne("Sandrine ", "Rousseau", 62, 52, DateTime.Now, "Mailles", "Hache"));
            EquipePoidsTresLegers.Add(new Personne("Jean-Luc ", "Mélenchon", 78, 59, DateTime.Now, "Plaques", "Hache"));

            EquipePoidsLeger.Add(new Personne("Xavier", "Bertrand", 55, 69, DateTime.Now, "Gambison", "Knaki"));
            EquipePoidsLeger.Add(new Personne("Eric", "Zemmour", 55, 75, DateTime.Now, "Mailles", "Cuillère"));
            EquipePoidsLeger.Add(new Personne("Nicolas", "Sarkozy", 55, 75, DateTime.Now, "Gambison", "Liasse de billets"));

            EquipePoidsLourds.Add(new Personne("Emmanuel", "Macron", 64, 85, DateTime.Now, "Plaques", "Couteau"));
            EquipePoidsLourds.Add(new Personne("Vladimir", "Poutine", 64, 92, DateTime.Now, "Gambison", "Masse"));
            EquipePoidsLourds.Add(new Personne("Édouard", "Philippe", 64, 81, DateTime.Now, "Gambison", "Masse"));

            EquipePoidsTresLourds.Add(new Personne("Jean", "Castex", 56, 103, DateTime.Now, "Plaques", "Katana"));
            EquipePoidsTresLourds.Add(new Personne("François", "Hollande", 28, 112, DateTime.Now, "Plaques", "Katana"));
            EquipePoidsTresLourds.Add(new Personne("Pierre", "Ménès", 56, 106, DateTime.Now, "Mailles", "Sabre"));
        }
    }
}
