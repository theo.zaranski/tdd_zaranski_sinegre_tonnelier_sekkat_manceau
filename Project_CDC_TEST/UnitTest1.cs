using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Project_CDC;

namespace Project_CDC_TEST
{

    [TestClass]
    public class UnitTest1
    {

        public static List<Personne> EquipePoidsTresLegers = new List<Personne>
        {
            new Personne("PrenomTestEquipePoidsTresLegers", "NomTest", 15,59, DateTime.Now, "Gambison", "Ep?e"),
            new Personne("PrenomTestEquipePoidsTresLegers", "Beccaro", 62,55, DateTime.Now, "Gambison", "Ep?e"),
            new Personne("PrenomTestEquipePoidsTresLegers", "Beccaro", 62,49, DateTime.Now, "Gambison", "Ep?e"),
        };
        public static List<Personne> EquipePoidsLegers = new List<Personne>
        {
            new Personne("PrenomTestEquipePoidsLeger", "Beccaro", 62,61, DateTime.Now, "Gambison", "Ep?e"),
            new Personne("PrenomTestEquipePoidsLeger", "Beccaro", 62,68, DateTime.Now, "Gambison", "Ep?e"),
            new Personne("PrenomTestEquipePoidsLeger", "Beccaro", 62,78, DateTime.Now, "Gambison", "Ep?e"),
        };
        public static List<Personne> EquipePoidsLourds = new List<Personne>
        {
            new Personne("PrenomTestEquipePoidsLourds", "Beccaro", 62,82, DateTime.Now, "Gambison", "Ep?e"),
            new Personne("PrenomTestEquipePoidsLourds", "Beccaro", 62,86, DateTime.Now, "Gambison", "Ep?e"),
            new Personne("PrenomTestEquipePoidsLourds", "Beccaro", 62,93, DateTime.Now, "Gambison", "Ep?e"),
        };
        public static List<Personne> EquipePoidsTresLourds = new List<Personne>
        {
            new Personne("PrenomTestEquipePoidsTresLourds", "Beccaro", 62,101, DateTime.Now, "Gambison", "Ep?e"),
            new Personne("PrenomTestEquipePoidsTresLourds", "Beccaro", 62,111, DateTime.Now, "Gambison", "Ep?e"),
            new Personne("PrenomTestEquipePoidsTresLourds", "Beccaro", 62,109, DateTime.Now, "Gambison", "Ep?e"),
        };

        [TestMethod]
        public void Personne_Possede_Bien_Une_Arme_Et_Une_Armure()
        {
            var test_personne = new Personne("PrenomTest", "NomTest", 35, 105, DateTime.Now, "Gambison", "?p?e");
            Assert.IsNotNull(test_personne.Arme);
            Assert.IsNotNull(test_personne.Armure);
        }

        [TestMethod]
        public void Chaque_Personnes_De_La_Categorie_Equipe_Poids_Tres_Legers_Possedent_De_Une_Armes()
        {
            for (int i = 0; i < EquipePoidsTresLegers.Count; i++)
            {
                if (EquipePoidsTresLegers[i].Poids < 60)
                {
                    Assert.IsNotNull(EquipePoidsTresLegers[i].Arme);
                }
                else
                {
                    Console.WriteLine("Error");
                }
            }
        }

        [TestMethod]
        public void Personne_De_56kg_Dans_La_Bonne_Categorie()
        {
            for (int i = 0; i < EquipePoidsTresLegers.Count; i++)
            {
                if (EquipePoidsTresLegers[i].Poids < 60)
                {
                    bool equipe = true;
                    Assert.IsTrue(equipe);
                }
                else
                {
                    bool equipe = false;
                    Assert.IsFalse(equipe);
                }
            }
        }

        [TestMethod]
        public void Test_Nom_Personnes()
        {
            string nom = EquipePoidsTresLourds[0].Nom;
            if (nom != null)
            {
                if ("Thierry" != nom)
                {
                    bool nom_personne = false;
                    Assert.IsFalse(nom_personne);
                }
                else if (nom == "Thierry")
                {
                    bool nom_personne = true;
                    Assert.IsTrue(nom_personne);
                }
            }
        }

        [TestMethod]
        public void Test_Prenom_Personne()
        {
            string prenom = EquipePoidsTresLourds[0].Prenom;
            if (prenom != null)
            {
                if ("Beccaro" != prenom)
                {
                    bool prenom_personne = false;
                    Assert.IsFalse(prenom_personne);
                }
                else if (prenom == "Beccaro")
                {
                    bool prenom_personne = true;
                    Assert.IsTrue(prenom_personne);
                }
            }
        }

        [TestMethod]
        public void Test_Age_Personne()
        {
            int age = EquipePoidsTresLourds[0].Age;
            if (age.ToString() != null)
            {
                if (62 != age)
                {
                    bool age_personne = false;
                    Assert.IsFalse(age_personne);
                }
                else if (age == 62)
                {
                    bool age_personne = true;
                    Assert.IsTrue(age_personne);
                }
            }
        }

        [TestMethod]
        public void Test_Poid_Personne()
        {
            int age = EquipePoidsTresLourds[0].Poids;
            if (age.ToString() != null)
            {
                if (62 != age)
                {
                    bool age_personne = false;
                    Assert.IsFalse(age_personne);
                }
                else if (age == 62)
                {
                    bool age_personne = true;
                    Assert.IsTrue(age_personne);
                }
            }
        }

        [TestMethod]
        public void Test_Armure_Personne()
        {
            string armure = EquipePoidsTresLourds[0].Armure;
            if (armure != null)
            {
                if (armure != "Gambison")
                {
                    bool armure_personne = false;
                    Assert.IsFalse(armure_personne);
                }
                else if (armure == "Gambison")
                {
                    bool armure_personne = true;
                    Assert.IsTrue(armure_personne);
                }
            }
        }

        [TestMethod]
        public void Test_Arme_Personne()
        {
            string arme = EquipePoidsTresLourds[0].Arme;
            if (arme != null)
            {
                if (arme != "Ep?e")
                {
                    bool arme_personne = false;
                    Assert.IsFalse(arme_personne);
                }
                else if (arme == "Ep?e")
                {
                    bool arme_personne = true;
                    Assert.IsTrue(arme_personne);
                }
            }
        }
        [TestMethod]
        public void Personne_De_69kg_Dans_La_Bonne_Categorie()
        {
            for (int i = 0; i < EquipePoidsLegers.Count; i++)
            {
                if (EquipePoidsLegers[i].Poids >= 60 && EquipePoidsLegers[i].Poids <= 80)
                {
                    bool equipe = true;
                    Assert.IsTrue(equipe);
                }
                else
                {
                    bool equipe = false;
                    Assert.IsFalse(equipe);
                }
            }
        }

        [TestMethod]
        public void Personne_De_87kg_Dans_La_Bonne_Categorie()
        {
            for (int i = 0; i < EquipePoidsLourds.Count; i++)
            {
                if (EquipePoidsLourds[i].Poids >= 80 && EquipePoidsLourds[i].Poids <= 100)
                {
                    bool equipe = true;
                    Assert.IsTrue(equipe);
                }
                else
                {
                    bool equipe = false;
                    Assert.IsFalse(equipe);
                }
            }
        }

        [TestMethod]
        public void Personne_De_105kg_Dans_La_Bonne_Categorie()
        {
            for (int i = 0; i < EquipePoidsTresLourds.Count; i++)
            {
                if (EquipePoidsTresLourds[i].Poids >= 100 && EquipePoidsTresLourds[i].Poids <= 120)
                {
                    bool equipe = true;
                    Assert.IsTrue(equipe);
                }
                else
                {
                    bool equipe = false;
                    Assert.IsFalse(equipe);
                }
            }
        }

        [TestMethod]
        public bool Difference_de_poids_acceptable_entre_equipes()
        {
            bool memepoids = false;
            bool acceptable = false;

            acceptable = Meme_Poids();

            if (memepoids = true)
            {
                return acceptable;
            }
            else
            {
                return acceptable;
            }

        }

        //M�thode appel� par le test du dessus
        public bool Meme_Poids()
        {
            bool equipe = false;
            for (int i = 0; i < EquipePoidsTresLourds.Count; i++)
            {
                if (EquipePoidsTresLourds[i].Poids >= 100 && EquipePoidsTresLourds[i].Poids <= 120)
                {
                    equipe = true;
                    Assert.IsTrue(equipe);
                }
                else
                {
                    equipe = false;
                    Assert.IsFalse(equipe);
                }
            }

            return equipe;
        }
        public void Verifier_Personne_Plus_De_16_Ans()
        {
            bool age_valide = true;
            int age = EquipePoidsTresLegers[0].Age;
            if (age.ToString() != null)
            {
                if (age < 16)
                {
                    Assert.IsFalse(age_valide);
                }
                else if (age >= 16)
                {
                    Assert.IsTrue(age_valide);
                }
            }
            age = EquipePoidsLegers[0].Age;
            if (age.ToString() != null)
            {
                if (age < 16)
                {
                    Assert.IsFalse(age_valide);
                }
                else if (age >= 16)
                {
                    Assert.IsTrue(age_valide);
                }
            }
        }

    }
}
