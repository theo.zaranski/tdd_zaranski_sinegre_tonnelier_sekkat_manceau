﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Project_CDC
{
   public class Personne 
   {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Age { get; set; }
        public int Poids { get; set; }
        public string Armure { get; set; }
        public string Arme { get; set; }
        public DateTime AnneeAdhesion { get; set; }



        public Personne(string nom, string prenom, int age, int poids, DateTime anneeAdhesion, string armure, string arme)
        {
            this.Nom = nom;
            this.Prenom = prenom;
            this.Age = age;
            this.Poids = poids;
            this.AnneeAdhesion = anneeAdhesion;
            this.Armure = armure;
            this.Arme = arme;
        }

   }
}
