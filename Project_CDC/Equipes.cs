﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_CDC
{
  public class Equipes
    {
        public  List<Personne> EquipePoidsTresLegers { get; set; }
        public  List<Personne> EquipePoidsLeger { get; set; }
        public  List<Personne> EquipePoidsLourds { get; set; }
        public  List<Personne> EquipePoidsTresLourds  { get; set; }
}
}
